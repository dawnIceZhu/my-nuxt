export const state = () => ({
  MY_NAME: 'dawnIceZhu',
  APP_PROJECT_NAME: 'dawnIceZhu\'s Notes',
  APP_PROJECT_DESC: 'People can skip meals for twelve days, But programming can’t stop writing for a day.'
})

export const getters = {}

export const mutations = {}

export const actions = {}
