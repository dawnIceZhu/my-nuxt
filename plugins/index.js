import Vue from 'vue'

/**
 * Vuesax
 */
import Vuesax from 'vuesax'
Vue.use(Vuesax)

/**
 * Cookies
 */
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
